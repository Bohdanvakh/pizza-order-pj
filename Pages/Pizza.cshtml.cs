using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PizzaOrder.Models;

namespace PizzaOrder.Pages
{
    public class PizzaModel : PageModel
    {
        public List<PizzasModel> fakePizzaDB = new List<PizzasModel>()
        {
            new PizzasModel(){
                ImageTitle="Pizza1",
                PizzaName="Pizza1",
                BasePrice=2,
                TomatoSauce=true,
                Cheese=true,
                FinalPrice=4},
            new PizzasModel(){
                ImageTitle="Pizza2",
                PizzaName="Pizza2",
                BasePrice=2,
                TomatoSauce=true,
                Cheese=true,
                FinalPrice=4},
            new PizzasModel(){
                ImageTitle="Pizza3",
                PizzaName="Pizza3",
                BasePrice=2,
                TomatoSauce=true,
                Cheese=true,
                FinalPrice=4},
            new PizzasModel(){
                ImageTitle="Pizza4",
                PizzaName="Pizza",
                BasePrice=2,
                TomatoSauce=false,
                Cheese=true,
                FinalPrice=3}
        };
        public void OnGet()
        {
        }
    }
}
